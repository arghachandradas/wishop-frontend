import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#7447ff',
    },
    secondary: {
      main: '##38d3a3',
    },
  },
});

export default theme;
