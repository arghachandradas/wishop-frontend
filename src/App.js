import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Header from './components/header/Header';
import { makeStyles } from '@material-ui/core/styles';
import Login from './pages/auth/Login';
import Register from './pages/auth/Register';
import Home from './pages/Home';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const useStyles = makeStyles((theme) => ({
  toolbar: theme.mixins.toolbar,
}));

const App = () => {
  const classes = useStyles();
  return (
    <>
      <Header />
      <div className={classes.toolbar} />
      <ToastContainer />
      <Switch>
        <Route exact path='/' component={Home} />
        <Route exact path='/login' component={Login} />
        <Route exact path='/register' component={Register} />
      </Switch>
    </>
  );
};

export default App;
