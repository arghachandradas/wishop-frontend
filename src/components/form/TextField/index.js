import React from 'react';
import { TextField } from '@material-ui/core';
import { useField } from 'formik';

const TextfieldWrapper = ({ name, ...otherProps }) => {
  const [field, metaData] = useField(name);

  const configTextfield = {
    ...field,
    ...otherProps,
    fullWidth: true,
    variant: 'outlined',
  };

  if (metaData && metaData.touched && metaData.error) {
    configTextfield.error = true;
    configTextfield.helperText = metaData.error;
  }

  return <TextField {...configTextfield} />;
};

export default TextfieldWrapper;
