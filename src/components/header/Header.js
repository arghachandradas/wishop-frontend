import React, { useState } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Drawer from '@material-ui/core/Drawer';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import PersonIcon from '@material-ui/icons/Person';
import HomeIcon from '@material-ui/icons/Home';
import MenuIcon from '@material-ui/icons/Menu';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

const useStyles = makeStyles((theme) => ({
  linkBtn: {
    textTransform: 'none',
    marginLeft: '1rem',
    '&:hover': {
      borderBottom: '1px solid #fff',
    },
  },
  drawer: {
    width: 300,
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
}));

const Header = () => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);
  const isMenuOpen = Boolean(anchorEl);
  const [mobileMenuOpen, setMobileMenuOpen] = useState(false);
  const [openSidenavDropdown, setOpenSidenavDropdown] = React.useState(false);

  const handleSidenavDropdownClick = () => {
    setOpenSidenavDropdown(!openSidenavDropdown);
  };

  const handleRegisterMenuOpen = (e) => {
    setAnchorEl(e.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const menuId = 'primary-search-account-menu';
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      id={menuId}
      keepMounted
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleMenuClose}>
        <ListItemIcon>
          <ExitToAppIcon fontSize='small' />
        </ListItemIcon>
        Logout
      </MenuItem>
    </Menu>
  );

  const renderMobileMenu = (
    <List component='nav' aria-labelledby='nested-list-subheader'>
      <ListItem
        button
        component={Link}
        to='/'
        onClick={() => setMobileMenuOpen(false)}
      >
        <ListItemIcon>
          <HomeIcon />
        </ListItemIcon>
        <ListItemText primary='Home' />
      </ListItem>
      <ListItem
        button
        component={Link}
        to='/login'
        onClick={() => setMobileMenuOpen(false)}
      >
        <ListItemIcon>
          <PersonIcon />
        </ListItemIcon>
        <ListItemText primary='Login' />
      </ListItem>
      <ListItem
        button
        component={Link}
        to='/register'
        onClick={() => setMobileMenuOpen(false)}
      >
        <ListItemIcon>
          <PersonAddIcon />
        </ListItemIcon>
        <ListItemText primary='Register' />
      </ListItem>
      <ListItem button onClick={handleSidenavDropdownClick}>
        <ListItemIcon>
          <AccountCircleIcon />
        </ListItemIcon>
        <ListItemText primary='Username' />
        {openSidenavDropdown ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={openSidenavDropdown} timeout='auto' unmountOnExit>
        <List component='div' disablePadding>
          <ListItem
            button
            className={classes.nested}
            onClick={() => setMobileMenuOpen(false)}
          >
            <ListItemIcon>
              <ExitToAppIcon />
            </ListItemIcon>
            <ListItemText primary='Logout' />
          </ListItem>
        </List>
      </Collapse>
    </List>
  );

  return (
    <>
      <Drawer
        open={mobileMenuOpen}
        onClose={() => setMobileMenuOpen(false)}
        anchor='right'
      >
        {renderMobileMenu}
      </Drawer>
      <AppBar color='primary'>
        <Toolbar>
          <Typography
            variant='h5'
            style={{ flexGrow: 1 }}
            component={Link}
            to='/'
            color='inherit'
          >
            WiShop
          </Typography>
          <div className={classes.sectionDesktop}>
            <Button
              color='inherit'
              component={Link}
              to='/'
              className={classes.linkBtn}
              startIcon={<HomeIcon />}
            >
              Home
            </Button>
            <Button
              color='inherit'
              component={Link}
              to='/login'
              className={classes.linkBtn}
              startIcon={<PersonIcon />}
            >
              Login
            </Button>
            <Button
              color='inherit'
              component={Link}
              to='/register'
              className={classes.linkBtn}
              startIcon={<PersonAddIcon />}
            >
              Register
            </Button>

            <Button
              aria-controls='simple-menu'
              aria-haspopup='true'
              onClick={handleRegisterMenuOpen}
              color='inherit'
              startIcon={<AccountCircleIcon />}
              className={classes.linkBtn}
            >
              Username
            </Button>
            {renderMenu}
          </div>
          <IconButton
            color='inherit'
            onClick={() => setMobileMenuOpen(true)}
            className={classes.sectionMobile}
          >
            <MenuIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
    </>
  );
};

export default Header;
