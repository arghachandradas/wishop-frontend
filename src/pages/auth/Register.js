import React from 'react';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import ButtonWrapper from '../../components/form/Button';
import TextFieldWrapper from '../../components/form/TextField';
import { toast } from 'react-toastify';
import { auth } from '../../firebase';

const useStyles = makeStyles((theme) => ({
  formWrapper: {
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(8),
  },
  textColor: {
    color: theme.palette.grey['A700'],
  },
}));

const INITIAL_FORM_STATE = {
  email: '',
};

const FORM_VALIDATION = Yup.object().shape({
  email: Yup.string().email('Invalid email.').required('Required'),
});

const Register = () => {
  const classes = useStyles();

  const handleSubmit = async (values, { resetForm }) => {
    const { email } = values;
    const config = {
      url: 'http://localhost:3000/register/complete',
      handleCodeInApp: true,
    };

    try {
      await auth.sendSignInLinkToEmail(email, config);
      toast.success(
        `Email is sent to ${email}. Click the link to complete your registration.`
      );
      // save email to localstorage
      localStorage.setItem('emailForRegistration', email);
      // clear form
      resetForm();
    } catch (error) {
      toast.error(error.message);
    }
  };

  return (
    <Grid item xs={12}>
      <Container maxWidth='sm'>
        <div className={classes.formWrapper}>
          <Formik
            initialValues={{
              ...INITIAL_FORM_STATE,
            }}
            validationSchema={FORM_VALIDATION}
            onSubmit={handleSubmit}
          >
            <Form>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <Typography
                    align='center'
                    variant='h5'
                    className={classes.textColor}
                  >
                    Register
                  </Typography>
                  <Typography
                    align='center'
                    variant='body1'
                    className={classes.textColor}
                  >
                    Create your account. It's free and only takes a minute.
                  </Typography>
                </Grid>
                <Grid item xs={12}>
                  <TextFieldWrapper name='email' label='Email' />
                </Grid>
                <Grid item xs={12}>
                  <ButtonWrapper
                    variant='contained'
                    color='primary'
                    fullWidth={true}
                  >
                    Submit Form
                  </ButtonWrapper>
                </Grid>
              </Grid>
            </Form>
          </Formik>
        </div>
      </Container>
    </Grid>
  );
};

export default Register;
