import React from 'react';
import Typography from '@material-ui/core/Typography';

const Home = () => {
  return (
    <div>
      <Typography variant='h1' color='primary'>
        Home
      </Typography>
    </div>
  );
};

export default Home;
