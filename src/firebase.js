import firebase from 'firebase/app';
import 'firebase/auth';

const firebaseConfig = {
  apiKey: 'AIzaSyBuZbzSzqj-5nF2_WJSWUL-wGIH9bwD1QQ',
  authDomain: 'wishop-f7ccf.firebaseapp.com',
  projectId: 'wishop-f7ccf',
  storageBucket: 'wishop-f7ccf.appspot.com',
  messagingSenderId: '991815049383',
  appId: '1:991815049383:web:205bcb1106f9da091d3b82',
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// export
export const auth = firebase.auth();
export const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
